;   Tic-Tac-Toe
;   play against the AI
;   intel 64-bit linux
;
;   by Johan Nestaas
;   johannestaas@gmail.com
;

        segment .data
p1:     db      0
p2:     db      0
x:      db      "X",0
o:      db      "O",0
e:      db      "_",0
endl:   db      0x0a,0
r:      dq      0
c:      dq      0
wrow:   db      0x0a,"Which row (0-2): ",0
wcol:   db      0x0a,"Which col (0-2): ",0
p1win:  db      0x0a,"You win!!!",0x0a,0
p2win:  db      0x0a,"You lose.",0x0a,0
tie:    db      0x0a,"Its a tie!",0x0a,0
row:    db      "%ld",0
col:    db      "%ld",0

    segment .text
    global  main
    extern  printf
    extern  scanf
main:
    push    rbp
    mov     rbp,        rsp
.move1:
    call    print_table
    call    ask_spot
    mov     rdi,        0       
    call    check_spot
    cmp     rax,        0
    jl      .move1           ; bad input
    call    check_win
    cmp     rax,        0
    jl      .end2
.move2:
    call    ai_choose
    call    check_win
    cmp     rax,        0
    jl      .end2
.move3:
    call    ask_spot
    mov     rdi,        0       
    call    check_spot
    cmp     rax,        0
    jl      .move3           ; bad input
    call    check_win
    cmp     rax,        0
    jl      .end2
.move4:
    call    ai_choose
    call    check_win
    cmp     rax,        0
    jl      .end2
.move5:
    call    ask_spot
    mov     rdi,        0       
    call    check_spot
    cmp     rax,        0
    jl      .move5           ; bad input
    call    check_win
    cmp     rax,        0
    jl      .end2
.move6:
    call    ai_choose
    call    check_win
    cmp     rax,        0
    jl      .end2
.move7:
    call    ask_spot
    mov     rdi,        0       
    call    check_spot
    cmp     rax,        0
    jl      .move7           ; bad input
    call    check_win
    cmp     rax,        0
    jl      .end2
    call    fill_last
.end:
    call    print_table
    call    check_win
    cmp     rax,        0
    jl      .end2
    lea     rdi,        [tie]
    xor     rax,        rax
    call    printf
.end2:
    leave
    ret

fill_last:
    push    rbp
    mov     rbp,        rsp
    mov     rax,        0
.check_each:
    bt      [p1],       rax
    jc      .end_check
    bt      [p2],       rax
    jc      .end_check
    bts     [p2],       rax
    leave
    ret
.end_check:
    inc     rax
    cmp     rax,        8
    jl      .check_each
    leave
    ret

;   error checking, writes results to memory
;   args:
;       rdi = player number 0 or 1
check_spot:
    push    rbp
    mov     rbp,        rsp
    mov     rax,        [r]
    mov     rbx,        [c]
    cmp     rax,        0
    jl      .bad_spot
    cmp     rax,        2
    jg      .bad_spot
    cmp     rbx,        0
    jl      .bad_spot
    cmp     rbx,        2
    jg      .bad_spot
    cmp     rax,        0
    jne     .sofar_sogood
    cmp     rbx,        0
    jne     .sofar_sogood
    jmp     .bad_spot
;   valid, 0-2 choices
.sofar_sogood:
    mov     rcx,        rax
    imul    rcx,        3
    add     rcx,        rbx
    dec     rcx
    bt      [p1],       rcx
    jc      .bad_spot
    bt      [p2],       rcx
    jc      .bad_spot
    cmp     rdi,        0
    jg      .setp2
.setp1:
    bts     [p1],       rcx
    jmp     .done
.setp2:
    bts     [p2],       rcx 
.done:
    mov     rax,        0
    leave
    ret
.bad_spot:
    mov     rax,        -1
    leave
    ret

ai_choose:
    push    rbp
    mov     rbp,        rsp
    cmp     rbx,        0
    cmovg   rax,        rbx
    cmp     rdx,        0
    jl      .skip_rdx
.ck_rdx:
    bt      [p1],       rdx
    jc      .skip_rdx
    bt      [p2],       rdx
    jc      .skip_rdx
    bts     [p2],       rdx
    jmp     .end
.skip_rdx:
    bt      [p2],       rax
    jc      .skip_set
    bt      [p1],       rax
    jc      .skip_set
    bts     [p2],       rax
    jmp     .end
.skip_set:
    call    fill_last
.end
    leave
    ret

;   ask which row and column
ask_spot:
    push    rbp
    mov     rbp,        rsp
    xor     rax,        rax
    lea     rdi,        [wrow]
    call    printf
    xor     rax,        rax
    lea     rdi,        [row]
    lea     rsi,        [r]
    call    scanf
    xor     rax,        rax
    lea     rdi,        [wcol]
    call    printf
    xor     rax,        rax
    lea     rdi,        [col]
    lea     rsi,        [c]
    call    scanf
    xor     rax,        rax
    leave
    ret

;   args:
;       rdi = which spot 0-8 from top to bottom
print_spot:
    push    rbp
    mov     rbp,        rsp
    bt      [p1],       rdi
    jc      .printo
    bt      [p2],       rdi
    jc      .printx
    lea     rdi,        [e]
    xor     rax,        rax
    call    printf
    leave
    ret
.printx:
    lea     rdi,        [x]
    xor     rax,        rax
    call    printf
    leave
    ret
.printo:
    lea     rdi,        [o]
    xor     rax,        rax
    call    printf
    leave
    ret

print_table:
    push    rbp
    mov     rbp,        rsp
    lea     rdi,        [endl]
    xor     rax,        rax
    call    printf
    lea     rdi,        [endl]
    xor     rax,        rax
    call    printf
    lea     rdi,        [x]
    xor     rax,        rax
    call    printf
    mov     rdi,        0
    call    print_spot
    mov     rdi,        1
    call    print_spot
    lea     rdi,        [endl]
    xor     rax,        rax
    call    printf
    mov     rdi,        2
    call    print_spot
    mov     rdi,        3
    call    print_spot
    mov     rdi,        4
    call    print_spot
    lea     rdi,        [endl]
    xor     rax,        rax
    call    printf
    mov     rdi,        5
    call    print_spot
    mov     rdi,        6
    call    print_spot
    mov     rdi,        7
    call    print_spot
    leave
    ret

check_win:
    push    rbp
    mov     rbp,        rsp
    call    print_table
.p1_r2:
    mov     rbx,        0
    mov     rdx,        -1
    mov     rax,        2
    bt      [p1],       rax
    jnc     .p1_r3
    mov     rax,        3
    bt      [p1],       rax
    jnc     .p1_r3
    mov     rax,        4
    bt      [p1],       rax
    jc      .p1_win
    bt      [p2],       rax
    mov     rcx,        0
    mov     rbx,        rax
    cmovc   rbx,        rcx
.p1_r3:
    mov     rax,        5
    bt      [p1],       rax
    jnc     .p1_c2
    mov     rax,        6
    bt      [p1],       rax
    jnc     .p1_c2
    mov     rax,        7
    bt      [p1],       rax
    jc      .p1_win     
    cmp     rbx,        0
    jg      .p1_c2
    bt      [p2],       rax
    mov     rcx,        0
    mov     rbx,        rax
    cmovc   rbx,        rcx
.p1_c2:
    mov     rax,        0
    bt      [p1],       rax
    jnc     .p1_c3
    mov     rax,        3
    bt      [p1],       rax
    jnc     .p1_c3
    mov     rax,        6
    bt      [p1],       rax
    jc      .p1_win
    cmp     rbx,        0
    jg      .p1_c3
    bt      [p2],       rax
    mov     rcx,        0
    mov     rbx,        rax
    cmovc   rbx,        rcx
.p1_c3:
    mov     rax,        1
    bt      [p1],       rax
    jnc     .p1_d2
    mov     rax,        4
    bt      [p1],       rax
    jnc     .p1_d2
    mov     rax,        7
    bt      [p1],       rax
    jc      .p1_win
    cmp     rbx,        0
    jg      .p1_d2
    bt      [p2],       rax
    mov     rcx,        0
    mov     rbx,        rax
    cmovc   rbx,        rcx
.p1_d2:
    mov     rax,        1
    bt      [p1],       rax
    jnc     .p2_r1
    mov     rax,        3
    bt      [p1],       rax
    jnc     .p2_r1
    mov     rax,        5
    bt      [p1],       rax
    jc      .p1_win
    cmp     rbx,        0
    jg      .p2_r1
    bt      [p2],       rax
    mov     rcx,        0
    mov     rbx,        rax
    cmovc   rbx,        rcx
.p2_r1:
    mov     rax,        0
    bt      [p2],       rax
    jnc     .p2_r2
    mov     rax,        1
    bt      [p2],       rax
    jc      .p2_win
    mov     rdx,        rax
.p2_r2:
    mov     rax,        2
    bt      [p2],       rax
    jnc     .p2_r3
    mov     rax,        3
    bt      [p2],       rax
    jnc     .p2_r3
    mov     rax,        4
    bt      [p2],       rax
    jc      .p2_win
    mov     rdx,        rax
.p2_r3:
    mov     rax,        5
    bt      [p2],       rax
    jnc     .p2_c1
    mov     rax,        6
    bt      [p2],       rax
    jnc     .p2_c1
    mov     rax,        7
    bt      [p2],       rax
    jc     .p2_win
    mov     rdx,        rax
.p2_c1:
    mov     rax,        2
    bt      [p2],       rax
    jnc     .p2_c2
    mov     rax,        5
    bt      [p2],       rax
    jc     .p2_win
    mov     rdx,        rax
.p2_c2:
    mov     rax,        0
    bt      [p2],       rax
    jnc     .p2_c3
    mov     rax,        3
    bt      [p2],       rax
    jnc     .p2_c3
    mov     rax,        6
    bt      [p2],       rax
    jc     .p2_win
    mov     rdx,        rax
.p2_c3:
    mov     rax,        1
    bt      [p2],       rax
    jnc     .p2_d1
    mov     rax,        4
    bt      [p2],       rax
    jnc     .p2_d1
    mov     rax,        7
    bt      [p2],       rax
    jc     .p2_win
    mov     rdx,        rax
.p2_d1:
    mov     rax,        3
    bt      [p2],       rax
    jnc     .p2_d2
    mov     rax,        7
    bt      [p2],       rax
    jc     .p2_win
    mov     rdx,        rax
.p2_d2:
    mov     rax,        1
    bt      [p2],       rax
    jnc     .nope
    mov     rax,        3
    bt      [p2],       rax
    jnc     .nope
    mov     rax,        5
    bt      [p2],       rax
    jc     .p2_win
    mov     rdx,        rax
.nope:
    leave
    ret
.p1_win:
    xor     rax,        rax
    lea     rdi,        [p1win]
    call    printf
    mov     rax,        -1
    leave
    ret
.p2_win:
    xor     rax,        rax
    lea     rdi,        [p2win]
    call    printf
    mov     rax,        -1
    leave
    ret
